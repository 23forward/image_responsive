<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// L
	'image_responsive_description' => 'Ce plugin propose de nouveaux filtres permettant de personnaliser finement et responsivement les images dans vos pages. Il n’aura d’intérêt donc que si vous modifiez vos squelettes pour l’utiliser selon vos besoins.
	<br>Le filtre ‘image_responsive’ (et ses paramètres) permet à vos images de s’adapter automatiquement à la taille et à la définition de l’écran du visiteur. 
	<br>Le filtre ‘image_proportions’, permet, par exemple, d’obtenir une image 16/9 responsive. L’option “_IMAGE_WEBP” automatise la fabrication d’images au format WebP pour les navigateurs compatibles.',
	'image_responsive_nom' => 'Image responsive',
	'image_responsive_slogan' => 'Filtres ‘image_responsive’ et ‘image_proportions’ pour vos squelettes',
);