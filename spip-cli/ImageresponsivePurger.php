<?php

namespace Spip\Cli\Command;

use Spip\Cli\Console\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ImageresponsivePurger extends Command {
	protected $sources = [];

	protected function configure() {
		$this
			->setName('imageresponsive:purger')
			->addOption(
				'from',
				null,
				InputOption::VALUE_OPTIONAL,
				'Purger les images plus non vues depuis cette date',
				''
			)
			->addOption(
				'dry-run',
				null,
				InputOption::VALUE_NONE,
				'Compter les images concernées par la purge sans les supprimer'
			)
			->setDescription('Purger les images responsives non vues depuis la date fournie dans --from')
		;
	}

	protected function execute(InputInterface $input, OutputInterface $output) {

		$from = $input->getOption('from');
		if (!$from || !strtotime($from)) {
			$this->io->error('Indiquez une date de péremption des images responsives à purger via l\'option --from');
			return self::FAILURE;
		}

		$from = strtotime($from);
		$this->io->title('Purger les images responsives non vues depuis : ' . date('Y-m-d H:i:s', $from));

		$dryrun = $input->getOption('dry-run');

		$purge_files = 0;
		$purge_size = 0;

		$dir_cache = _DIR_VAR . 'cache-responsive/';
		$subdir_caches = glob($dir_cache . '*', GLOB_ONLYDIR);
		foreach ($subdir_caches as $subdir_cache) {
			$subdir_cache = rtrim($subdir_cache, '/') . '/';
			$images = glob($subdir_cache . '*');
			foreach ($images as $image) {
				if (fileatime($image) < $from) {
					$purge_files++;
					$purge_size += filesize($image);
					if (!$dryrun) {
						$this->io->care("Suppression $image");
						@unlink($image);
					}
				}
			}
		}

		if (!$purge_files) {
			$this->io->care("Rien à purger");
		} else {
			include_spip('inc/filtres');
			if ($dryrun) {
				$this->io->care("$purge_files fichiers à purger (" . taille_en_octets($purge_size).')');
			} else {
				$this->io->success("$purge_files fichiers purgés (" . taille_en_octets($purge_size).')');
			}
		}

		return self::SUCCESS;
	}

}