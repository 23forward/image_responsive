<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function image_responsive_calculer_cache() {
		$now = time();

		$supprimer = _request("supprimer");

		$ages = array(3, 7, 15, 30, 92);
		$tailles = $durees = array();
		$tailles["tout"] = 0;

		foreach ($ages as $age) {
			$durees[$age] = 60*60*24*$age;
			$tailles[$age] = 0;
		} 

		$base = sous_repertoire(_DIR_VAR, "cache-responsive");
		$d = dir($base);

		while (false !== ($entry = $d->read())) {
			$sousdir = "$base$entry";
			if (substr($entry, 0, 1) != "." &&  is_dir($sousdir)) {

				$dd = dir($sousdir);
				while (false !== ($f = $dd->read())) {
					if (substr($f, 0, 1) != ".") {
						$fichier = "$sousdir/$f";

						//echo "<li><a href='$fichier'>$fichier</a>";
						if (file_exists($fichier)) {
							$fsize = filesize($fichier);
							$fat = @fileatime($fichier);

							if ($supprimer == "tout") {
								@unlink($fichier);
							} else {
								$tailles["tout"] += $fsize;
							}

							if ($fat) {
								$age = $now - $fat;

								foreach ($durees as $jours => $duree) {
									if ($age > $duree) {
										if ($supprimer == $jours) {
											@unlink($fichier);
										} else {
											$tailles[$jours] += $fsize;
										}
									}
								}

							}
						}
					}
				}
				$dd->close();
			}
		
		}
		$d->close();

		include_spip("inc/filtres");

		if (!$supprimer) {
			echo "<table class='spip'>";
			$supp = bouton_action("Supprimer", parametre_url(self(), 'supprimer', 'tout'), 'btn btn_mini btn_danger');
			echo "<tr><td><b>Toutes les images</b></td> <td> ". (taille_en_octets($tailles["tout"]) ?: '-')."</td><td>$supp</td></tr>";

			foreach ([3 => '3 jours',7 => 'une semaine',15 => '15 jours',30 => 'un mois',92 => 'trois mois'] as $age => $texte) {
				if (!empty($tailles["$age"])) {
					$supp = bouton_action("Supprimer", parametre_url(self(), 'supprimer', $age), 'btn btn_mini btn_danger');
					echo "<tr><td>Non vues depuis $texte</td> <td> ". (taille_en_octets($tailles["$age"]) ?: '-')."</td><td>$supp</td></tr>";
				}
			}
			echo "</table>";
		} else {
			header("Location:index.php?exec=admin_vider");
			exit;
		}
}